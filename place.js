var places = new Ext.Application({
    name: 'places',
    useLoadMask: true,
    launch: function () {

        places.views.ptp = new Ext.Toolbar({
        		baseCls: 'toolbar',
				items: [
					{
						html: 'logo',
						baseCls: 'logo'
					},
					{
						html: 'Map',
						baseCls: 'topbuts active'
					},
					{
						html: 'Items',
						baseCls: 'topbuts',
						handler: function() {
							places.views.viewport.setActiveItem('contitems', {type: 'slide', direction: 'left'});							
						}
					}				
				]        
        });

        places.views.ptp2 = new Ext.Toolbar({
        		baseCls: 'toolbar',
				items: [
					{
						html: 'logo',
						baseCls: 'logo'
					},
					{
						html: 'Map',
						baseCls: 'topbuts',
						handler: function() {		
							places.views.viewport.setActiveItem('contmap', {type: 'slide', direction: 'right'});			
						}
					},
					{
						html: 'Items',
						baseCls: 'topbuts active'
					}				
				]        
        });
			
        places.views.mapp = new Ext.Map({
    			mapOptions : {
					zoom : 12,
                mapTypeId : google.maps.MapTypeId.HYBRID,
                navigationControl: true,
                navigationControlOptions: {
                   style: google.maps.NavigationControlStyle.DEFAULT
                }    			
    			},
    			geo: geog = new Ext.util.GeoLocation({
      			autoUpdate:true,
      			allowHighAccuracy: false,
			      /*listeners:{
		         	locationupdate: function(geo) {
			        	  center = new google.maps.LatLng(geo.latitude, geo.longitude);
         			},
        				locationerror: function(geo){
          				alert('got geo error');          
        				}
      			}*/
    			}),
    			useCurrentLocation: true,
    		  plugins : [
             new Ext.plugin.GMap.Tracker({
                    trackSuspended : true,   //suspend tracking initially
                    highAccuracy   : false,
                    marker : new google.maps.Marker({
                       title : 'Where I am',
                       draggable: true,
                       position: new google.maps.LatLng(geog.latitude, geog.longtitude)
                       //shadow: shadow,
                       //icon  : image
                   })
              })
              //new Ext.plugin.GMap.Traffic({ hidden : false })
            ]
        });        
        
        places.views.items = new Ext.Panel({
				id: 'items',
				layout: 'fit',
				title: 'Items',
				html: 'items'        
        });
    	
    	  places.views.contmap = new Ext.Panel({
				id: 'contmap',
				layout: 'fit',
				title: 'Places',
				dockedItems: [places.views.ptp, places.views.mapp]    	  
    	  });
    	
    	  places.views.contitems = new Ext.Panel({
				id: 'contitems',
				layout: 'fit',
				title: 'Places',
				dockedItems: [places.views.ptp2, places.views.items]    	  
    	  });

        places.views.viewport = new Ext.Panel({
            fullscreen : true,
            layout : 'card',
            cardAnimation : 'slide',
            items: [places.views.contmap,
            places.views.contitems]
        });
    }
})
