ascom.views.MapG = Ext.extend(Ext.Panel, {

layout:'card',
initComponent: function(){

    var infowindow = new google.maps.InfoWindow({
        content: 'prova'
    });

    this.map = new Ext.Map({
        mapOptions : {
            zoom: 12,
            navigationControlOptions: {
                style: google.maps.NavigationControlStyle.DEFAULT
            }
        },
        useCurrentLocation: true,
        listeners: {
            maprender : function(comp, map){
                var marker = new google.maps.Marker({
                     position: map.center,
                     title : 'Infofactory HQ',
                     map: map
                });

                infowindow.open(map, marker);

                google.maps.event.addListener(marker, 'click', function() {
                     infowindow.open(map, marker);
                });
            }
        }
    });

    this.panel = new Ext.Panel({
        layout:'fit',
        items:this.map,
        dockedItems:[
            {
                xtype:'toolbar',
                title:'Map GO'
            }
        ]
    });

    this.items = this.panel;
    ascom.views.MapG.superclass.initComponent.call(this);

}


});